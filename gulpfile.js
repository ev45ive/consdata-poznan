var gulp = require('gulp')
var gulpConcat = require('gulp-concat')
var gulpUglify = require('gulp-uglify')
var gulprimraf = require('gulp-rimraf')
var gulpSourcemaps = require('gulp-sourcemaps')
var babel = require("gulp-babel");

var browserSync = require('browser-sync').create()
var reload = browserSync.reload

var paths = {
  js: './src/**/*.js'
}

gulp.task('default', function () {
  console.log('hello')
})

gulp.task('serve', function () {
  browserSync.init({
    server: './',
    port: 8080
  })
  gulp.watch([paths.js], ['build'])
  gulp.watch([paths.js]).on('change', reload )
})

gulp.task('watch', function () {
  gulp.watch([paths.js], ['build'])
})

gulp.task('build', function () {

  gulp.src([paths.js])
    .pipe(gulpSourcemaps.init())
    .pipe(babel({
      "presets": ["env"]
    }))
    .pipe(gulpConcat('bundle.js'))
    // .pipe(gulpUglify())
    .pipe(gulpSourcemaps.write())
    .pipe(
    gulp.dest('dist')
    )

})