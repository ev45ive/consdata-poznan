

function Cart(products) {
  this.products = products
  this.total = 0
}

Cart.prototype.addProduct = function (product) {
  // filter
  var found = this.products.filter(function (p) {
    return p.name === product.name
  })
  found = found[0]

  // update quantity or add
  if (found) {
    found.quantity += 1
  } else {
    this.products.push(product)
  }
}

Cart.prototype.removeProduct = function (product) {
  // filter
  var found = this.products.filter(function (p) { return p.name === product.name })
  found = found[0]

  // update quantity or remove
  if (found) {
    found.quantity -= 1
    if (found.quantity <= 0) {
      var index = this.products.indexOf(found)
      this.products.splice(index, 1)
    }
  }
}

Cart.prototype.getSubtotal = function (product) {
  return product.price * product.quantity
}

Cart.prototype.recalculate = function () {
  // var that = this;

  var subTotalReducer = function (sum, product) {
    return sum += this.getSubtotal(product)
  }

  this.total = this.products.reduce(subTotalReducer.bind(this), 0)
}

Cart.prototype.render = function () {
  var output = []
  for (var i in this.products) {
    var product = products[i]

    var promo = product.promo == true;
    var subtotal = this.getSubtotal(product).toFixed(2)

    output.push([product.name, 'x', product.quantity, '=', subtotal, promo ? 'PROMOTION' : ''].join(' '));
  }
  this.recalculate()
  console.log('==================')
  console.log(output.join('\n\r'))
  console.log('==================')
  console.log('Total = ' + this.total)
}