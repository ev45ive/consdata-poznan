
fdescribe('Product Form', function(){
  var form, element

  beforeEach(function(){
    element = document.createElement('form')

    form = new ProductForm(element)
  })

  it('should call getData on submit',function(){
    spyOn(form,'getData').and.returnValue(123)
    
    element.dispatchEvent(new Event('submit'))
    expect(form.getData).toHaveBeenCalled()
  })

})