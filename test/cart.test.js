
describe('Shopping Cart', function(){
  var cart

  beforeEach(function(){
    cart = new Cart([{name:'Test'}])
  })

  it('should test', function(){
    expect(true).toBeTruthy()
  })
  
  it('initializes with list of products',function(){

    // debugger;
    expect(cart.products.length).toEqual(1)
    expect(cart.products).toEqual([{name:'Test'}])

  })
  
    
})