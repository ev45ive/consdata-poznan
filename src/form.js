
function ProductForm(elem) {
  this.elem = elem

  this.elem.addEventListener('submit', this.onSubmit.bind(this))
}

ProductForm.prototype = {
  getData: function () {

    return {
      name: this.getFieldValue('name'),
      price: this.getFieldValue('price'),
      promo: this.getFieldValue('promo'),
    }
  },
  onSubmit: function (event) {
    event.preventDefault()
    console.log(this.getData())
  },
  getFieldValue: function (name) {
    var field = this.getField(name)

    if (['checkbox', 'radio'].includes(field.type)) {
      return field.checked
    } else {
      return field.value
    }
  },
  getField: function (name) {
    // return this.elem.querySelector('[name="'+name+'"]')

    // Tylko jesli  wewnatrz elementu <form>:
    return this.elem.elements[name]
  }
}